#include "6x13.pcf.gz.h"

#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>

#include <iostream>
#include <string>
#include <vector>

unsigned int quit = 0;
std::string txtConsole;

void udpStuff()
{
    std::cout << "App launched..." << std::endl;

    sf::UdpSocket dataFeedSocket;
    dataFeedSocket.bind(28282);
    char buffer[1024];

    dataFeedSocket.setBlocking(false);
    std::size_t received = 0;
    sf::IpAddress sender;
    unsigned short port;

    sf::Socket::Status status;
    while (!quit) {
        status = dataFeedSocket.receive(buffer, sizeof(buffer), received, sender, port);
        if (status != sf::Socket::NotReady) {
            txtConsole.append(buffer);
        }
    }
}

int main()
{
    sf::RenderWindow window((sf::VideoMode::getFullscreenModes()[0]), "SFML works!", sf::Style::Fullscreen);
    sf::CircleShape shape(100.f);
    shape.setFillColor(sf::Color::Green);
    auto x = window.getSize();
    std::cout << "Window size: " << x.x << ", " << x.y << std::endl;
    sf::Font font;
    font.loadFromMemory(x11_6x13_pcf_gz, x11_6x13_pcf_gz_len); ///usr/share/fonts/X11/misc/6x13.pcf.gz

    sf::RectangleShape consoleWindow();
    quit = 0;

    txtConsole.append("Initializing instrumentation repeater...\n");

    // Display the list of all the video modes available for fullscreen
    std::vector<sf::VideoMode> modes = sf::VideoMode::getFullscreenModes();
    for (std::size_t i = 0; i < modes.size(); ++i)
    {
        sf::VideoMode mode = modes[i];
        /*
        std::cout << "Mode #" << i << ": "
                << mode.width << "x" << mode.height << " - "
                << mode.bitsPerPixel << " bpp" << std::endl;
        */
    }

    sf::Thread dataFeedThread(&udpStuff);
    dataFeedThread.launch();



    int tik = 0;

    sf::Text text;
    text.setFont(font);
    text.setCharacterSize(13);
    text.setFillColor(sf::Color::White);
    //text.setstyle
    while (window.isOpen())
    {
        tik++;

        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::KeyPressed) {
                txtConsole.append("key pressed: ");
                txtConsole.append(std::to_string(event.key.code));
                txtConsole.append("\n");
                if (event.key.code == 16U)
                    window.close();
            }
            if (event.type == sf::Event::Closed)
                window.close();
        }
                shape.setPosition(0.f + (tik % 900), 100.f + (tik % 900));

        window.clear(sf::Color(64, 64, 64));
        window.draw(shape);
        text.setString(txtConsole);
        window.draw(text);
        window.display();
    }

    quit = 1;
    return 0;
}
